let uniqueId = 0;

const allElements = document.getElementById("allElements");
const activeElements = document.getElementById("activeElements");
const completedDiv = document.getElementById("completedDiv");
const searchBar = document.getElementById("searchBar");

const buttonAll = document.getElementById("buttonAll");
const buttonActive = document.getElementById("buttonActive");
const buttonCompleted = document.getElementById("buttonCompleted");

let once = 0;
if (once == 0) {
    getAll();
    once++;
}

const inputTask = document.getElementById("inputId");
const completedElements = document.getElementById("completedElements");

inputTask.addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
        addToDoTask();
    }
})