const getAll = () => {
    allElements.style.display = 'block';
    activeElements.style.display = 'none';
    completedDiv.style.display = 'none';
    searchBar.style.display = 'block';
    buttonAll.style.borderBottom = "4px solid #2F80ED";
    buttonActive.style.borderBottom = "none";
    buttonCompleted.style.borderBottom = "none";
}

const getActive = () => {
    allElements.style.display = 'none';
    activeElements.style.display = 'block';
    completedDiv.style.display = 'none';
    searchBar.style.display = 'block';
    buttonAll.style.borderBottom = "none";
    buttonActive.style.borderBottom = "4px solid #2F80ED";
    buttonCompleted.style.borderBottom = "none";
}

const getCompleted = () => {
    allElements.style.display = 'none';
    activeElements.style.display = 'none';
    completedDiv.style.display = 'block';
    searchBar.style.display = 'none';
    buttonAll.style.borderBottom = "none";
    buttonActive.style.borderBottom = "none";
    buttonCompleted.style.borderBottom = "4px solid #2F80ED";
}

const addToDoTask = () => {
    const inputIdValue = inputTask.value;
    if (inputIdValue === "") {
        alert("Please enter detail");
    } else {
        allElements.innerHTML += `
        <div id="cardAll${uniqueId}" class="card">
            <input onclick="addInCompleted(${uniqueId})" class="checkbox" type="checkbox" name="" id="checkbox${uniqueId}">
        
            <div id="text${uniqueId}">
                <p>${inputIdValue}</p>
            </div>
        </div>`

        activeElements.innerHTML += `
        <div id="cardActive${uniqueId}" class="card ">
            <input onclick="addInCompleted(${uniqueId})" class="checkbox" type="checkbox" name="" id="">
            <div id="text${uniqueId}">
                <p>${inputIdValue}</p>
            </div>
        </div>`
        uniqueId++;
        inputTask.value = "";

        const completedElementsChildren = completedElements.children;
        for (let i = 0; i < completedElementsChildren.length; i++) {
            const childId = completedElementsChildren[i]["id"].substring(13);
            const checkBox = document.getElementById(`checkbox${childId}`);
            checkBox.checked = true
        }

    }
}

const addInCompleted = (Id) => {
    const childOfSectionDiv = document.getElementById(`checkbox${Id}`);
    if (childOfSectionDiv.classList.contains("checkedClasss")) {
        childOfSectionDiv.classList.remove("checkedClasss");
        const InputText = document.getElementById(`text${Id}`).innerText;
        document.getElementById(`text${Id}`).classList.remove("lineThrough");

        activeElements.innerHTML += `
        <div id="cardActive${Id}" class="card ">
            <input onclick="addInCompleted(${Id})" class="checkbox" type="checkbox" name="" id="">
            <div id="text${Id}">
                <p>${InputText}</p>
            </div>
        </div>`
        const divForRemove = document.getElementById(`cardCompleted${Id}`);
        divForRemove.remove();
        const checkBox = document.getElementById(`checkbox${Id}`)
        checkBox.checked = false

    } else {
        childOfSectionDiv.classList.add("checkedClasss");
        const InputText = document.getElementById(`text${Id}`).innerText;
        document.getElementById(`text${Id}`).classList.add("lineThrough");

        const childDiv = `
        <div id="cardCompleted${Id}" class="card deleteicon">
            <div class="flex"> 
                <input onclick="addInCompleted(${Id})" type="checkbox" name="" id="alwaysChecked${Id}" class="checkbox">
                <p class="lineThrough">${InputText}</p>           
            </div>
            <div>
                <i onclick="deleteDiv(${Id})" class="fa fa-trash" aria-hidden="true"></i>
            </div>
        </div>`
        completedElements.innerHTML += childDiv;

        const completedElementsChildren = completedElements.children
        for (let i = 0; i < completedElementsChildren.length; i++) {
            const childId = completedElementsChildren[i]["id"]
            const checkBox = document.getElementById(`alwaysChecked${childId.substring(13)}`)
            checkBox.checked = true
        }
        const divForRemove = document.getElementById(`cardActive${Id}`);
        divForRemove.remove();
        const checkBox = document.getElementById(`checkbox${Id}`)
        checkBox.checked = true
    }
}

const deleteAll = () => {
    const completedElementsChildren = completedElements.children;
    for (let child = 0; child < completedElementsChildren.length; child++) {
        const childId = completedElementsChildren[child]["id"]
        const divForRemove = document.getElementById(`cardAll${childId.substring(13)}`);
        divForRemove.remove();
    }
    completedElements.innerHTML = "";
}

const deleteDiv = (Id) => {
    const divForRemove = document.getElementById(`cardCompleted${Id}`);
    const selectionDivForRemove = document.getElementById(`cardAll${Id}`);
    divForRemove.remove();
    selectionDivForRemove.remove();
}